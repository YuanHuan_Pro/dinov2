# Copyright (c) Meta Platforms, Inc. and affiliates.
import cv2
import numpy as np
from PIL import Image

import mmcv
from mmcv.runner import load_checkpoint
from mmseg.apis import init_segmentor, inference_segmentor

import torch.nn.functional as F

import sys
sys.path.insert(0, '/yuanhuan/code/demo/Image/pretrain/dinov2')
import dinov2.eval.segmentation.utils.colormaps as colormaps
import dinov2.eval.segmentation_m2f.models.segmentors

DATASET_COLORMAPS = {
    "ade20k": colormaps.ADE20K_COLORMAP,
    "voc2012": colormaps.VOC2012_COLORMAP,
}

def render_segmentation(segmentation_logits, dataset):
    colormap = DATASET_COLORMAPS[dataset]
    colormap_array = np.array(colormap, dtype=np.uint8)
    segmentation_values = colormap_array[segmentation_logits + 1]
    return Image.fromarray(segmentation_values)

image = Image.open("/yuanhuan/data/image/RM_C28_safeisland/original_road_seg/zhongdong/JPEGImages/0000000000000000-230109-102848-102856-00020A000011_00000.jpg")
image_rgb = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)
cv2.imwrite("/yuanhuan/code/demo/Image/pretrain/dinov2/notebooks/image.jpg", image_rgb)

# Semantic segmentation on sample image
cfg = mmcv.Config.fromfile("/yuanhuan/model/image/dinov2/meta/checkpoints/dinov2_vitg14_ade20k_m2f_config.py")

model = init_segmentor(cfg)
load_checkpoint(model, "/yuanhuan/model/image/dinov2/meta/checkpoints/dinov2_vitg14_ade20k_m2f.pth", map_location="cpu")
model.cuda()
model.eval()

## Semantic segmentation on sample image
array = np.array(image)[:, :, ::-1] # BGR
segmentation_logits = inference_segmentor(model, array)[0]
segmented_image = render_segmentation(segmentation_logits, "ade20k")
segmented_image_rgb = cv2.cvtColor(np.asarray(segmented_image), cv2.COLOR_RGB2BGR)
cv2.imwrite("/yuanhuan/code/demo/Image/pretrain/dinov2/notebooks/segmented_image_Mask2Former.jpg", segmented_image_rgb)